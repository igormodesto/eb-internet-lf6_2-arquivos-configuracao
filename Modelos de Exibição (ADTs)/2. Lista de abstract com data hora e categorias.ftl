﻿<#--
	############################
	## Parâmetros do template ##
	############################
-->
<#assign titulo_lista = "Noticiário do Exercito" /> <#-- titulo da listagem de conteúdos -->
<#assign portlet_display = themeDisplay.getPortletDisplay() />
<#assign portlet_title = htmlUtil.escape(portlet_display.getTitle()) />
<div class="titulo-sem-borda">
    <header class="portlet-topper">
        <h1 class="portlet-title">
            <span class="portlet-title-text">
                ${portlet_title} 
            </span>
        </h1> 
    </header>
</div>
<#assign liferay_ui = taglibLiferayHash["/WEB-INF/tld/liferay-ui.tld"] />


<#list entries as entry>
	<#assign entry = entry />

	<#assign assetRenderer = entry.getAssetRenderer() />

	<#assign entryTitle = htmlUtil.escape(assetRenderer.getTitle(locale)) />

	<#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, entry) />

	<#if assetLinkBehavior != "showFullContent">
		<#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, viewURL) />
	</#if>

	<#assign res =  viewURL?matches("(.*?)\\?.*")>
	<#assign viewURL =  res?groups[1] />
	
    <#assign assetCategoryLocalServiceUtil = serviceLocator.findService("com.liferay.portlet.asset.service.AssetCategoryLocalService") />
    
    <#assign assetVocabularyLocalServiceUtil = serviceLocator.findService("com.liferay.portlet.asset.service.AssetVocabularyLocalService") />
    
    <#assign articleResourcePrimKey = entry.getAssetRenderer().getArticle().getResourcePrimKey() />
    <#assign temasVocabularyId = 0 />
	<#assign tiposVocabularyId = 0 />
    
    <#list assetVocabularyLocalServiceUtil.getAssetVocabularies(-1, -1) as vocabulary>
        <#if vocabulary.getName() == "Temas">
            <#assign temasVocabularyId = vocabulary.getVocabularyId() />
        </#if>
		<#if vocabulary.getName() == "Tipos">
            <#assign tiposVocabularyId = vocabulary.getVocabularyId() />
        </#if>
    </#list>
	
    <#assign dateFormat2 = "dd/MM/yy" />

	<#assign dateFormat3 = "HH:mm" />

	<div class="asset-abstract">
		<div class="row-fluid">
			
			<div class="detalhes span2">
				<div class="data">
					<i class="icon-calendar"></i>
					${dateUtil.getDate(entry.getPublishDate(), dateFormat2, locale)}
				</div>
				<div class="hora">
					<i class="icon-time"></i>
					${dateUtil.getDate(entry.getPublishDate(), dateFormat3, locale)}
				</div>
                <#list assetCategoryLocalServiceUtil.getCategories("com.liferay.portlet.journal.model.JournalArticle", articleResourcePrimKey) as item>
                    <#if item.getVocabularyId() == tiposVocabularyId>
                    	<div class="tipos">
                    		<#if item.name?matches("not[íi]?cias?", "i")>
								<i class="icon-align-left"></i>
                        	</#if>
                        	<#if item.name?matches("v[íi]?deos?", "i")>
								<i class="icon-facetime-video"></i>
                        	</#if>
                        	<#if item.name?matches("[áaÁA]?udios?", "i")>
								<i class="icon-volume-up"></i>
                        	</#if>
                        	<#if item.name?matches("documentos?", "i")>
								<i class="icon-folder-open"></i>
                        	</#if>
                        	${item.name}
                        </div>
                    </#if>
                </#list>
			</div>
			<div class="resumo span10">
				<div class="container-chapeu"> 
	                <#list assetCategoryLocalServiceUtil.getCategories("com.liferay.portlet.journal.model.JournalArticle", articleResourcePrimKey) as item>
	                    <#if item.getVocabularyId() == temasVocabularyId>
	                    	<div class="chapeu">
	                        	${item.getName()}
	                        </div>
	                    </#if>
	                </#list>
				</div>
				<div class="imagem">
						<#if entry.getAssetRenderer().getThumbnailPath(renderRequest)?contains("/images/file_system/large/article.png") >
							
						<#else>

							<a href="${viewURL}">
								<img src="${entry.getAssetRenderer().getThumbnailPath(renderRequest)}" alt="${entry.getTitle(locale)}" />
							</a>

						</#if>
				</div>
				<div class="sumario">
					
					<h3 class="asset-title">
						<a href="${viewURL}">${entryTitle}</a>
					</h3>
					<div class="asset-summary">

						${htmlUtil.escape(assetRenderer.getSummary(locale))}

					</div>
					<div class="tags">
						<@getMetadataField fieldName="tags" />
					</div>
				</div>
			</div>	
			<div class="lfr-meta-actions asset-actions">
				<@getEditIcon />
			</div>	
		</div>	
	</div>

</#list>

<#macro getDiscussion>
	<#if validator.isNotNull(assetRenderer.getDiscussionPath()) && (enableComments == "true")>
		<br />

		<#assign discussionURL = renderResponse.createActionURL() />

		${discussionURL.setParameter("struts_action", "/asset_publisher/" + assetRenderer.getDiscussionPath())}

		<@liferay_ui["discussion"]
			className=entry.getClassName()
			classPK=entry.getClassPK()
			formAction=discussionURL?string
			formName="fm" + entry.getClassPK()
			ratingsEnabled=enableCommentRatings == "true"
			redirect=portalUtil.getCurrentURL(request)
			userId=assetRenderer.getUserId()
		/>
	</#if>
</#macro>

<#macro getEditIcon>
	<#if assetRenderer.hasEditPermission(themeDisplay.getPermissionChecker())>
		<#assign redirectURL = renderResponse.createRenderURL() />

		${redirectURL.setParameter("struts_action", "/asset_publisher/add_asset_redirect")}
		${redirectURL.setWindowState("pop_up")}

		<#assign editPortletURL = assetRenderer.getURLEdit(renderRequest, renderResponse, windowStateFactory.getWindowState("pop_up"), redirectURL)!"" />

		<#if validator.isNotNull(editPortletURL)>
			<#assign title = languageUtil.format(locale, "edit-x", entryTitle) />

			<@liferay_ui["icon"]
				image="edit"
				message=title
				url="javascript:Liferay.Util.openWindow({dialog: {width: 960}, id:'" + renderResponse.getNamespace() + "editAsset', title: '" + title + "', uri:'" + htmlUtil.escapeURL(editPortletURL.toString()) + "'});"
			/>
		</#if>
	</#if>
</#macro>

<#macro getFlagsIcon>
	<#if enableFlags == "true">
		<@liferay_ui["flags"]
			className=entry.getClassName()
			classPK=entry.getClassPK()
			contentTitle=entry.getTitle(locale)
			label=false
			reportedUserId=entry.getUserId()
		/>
	</#if>
</#macro>

<#macro getMetadataField
	fieldName
>
	<#if stringUtil.split(metadataFields)?seq_contains(fieldName)>
		<span class="metadata-entry metadata-"${fieldName}">
			<#assign dateFormat = "dd MMM yyyy - HH:mm:ss" />
			<#assign dateFormat2 = "dd/mm/yy" />
			<#assign dateFormat3 = "HH:mm:ss" />

			<#if fieldName == "author">
				<@liferay.language key="by" /> ${portalUtil.getUserName(assetRenderer.getUserId(), assetRenderer.getUserName())}
			<#elseif fieldName == "categories">
				<@liferay_ui["asset-categories-summary"]
					className=entry.getClassName()
					classPK=entry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif fieldName == "create-date">
				${dateUtil.getDate(entry.getCreateDate(), dateFormat, locale)}
			<#elseif fieldName == "expiration-date">
				${dateUtil.getDate(entry.getExpirationDate(), dateFormat, locale)}
			<#elseif fieldName == "modified-date">
				${dateUtil.getDate(entry.getModifiedDate(), dateFormat, locale)}
			<#elseif fieldName == "publish-date">
				${dateUtil.getDate(entry.getPublishDate(), dateFormat2, locale)}
			<#elseif fieldName == "priority">
				${entry.getPriority()}
			
			<#elseif fieldName == "tags">
				<@liferay_ui["asset-tags-summary"]
					className=entry.getClassName()
					classPK=entry.getClassPK()
					portletURL=renderResponse.createRenderURL()
				/>
			<#elseif fieldName == "view-count">
				<@liferay_ui["icon"]
					image="history"
				/>

				${entry.getViewCount()} <@liferay.language key="views" />
			</#if>
		</span>
	</#if>
</#macro>

<#macro getPrintIcon>
	<#if enablePrint == "true" >
		<#assign printURL = renderResponse.createRenderURL() />

		${printURL.setParameter("struts_action", "/asset_publisher/view_content")}
		${printURL.setParameter("assetEntryId", entry.getEntryId()?string)}
		${printURL.setParameter("viewMode", "print")}
		${printURL.setParameter("type", entry.getAssetRendererFactory().getType())}

		<#if (validator.isNotNull(assetRenderer.getUrlTitle()))>
			<#if (assetRenderer.getGroupId() != themeDisplay.getScopeGroupId())>
				${printURL.setParameter("groupId", assetRenderer.getGroupId()?string)}
			</#if>

			${printURL.setParameter("urlTitle", assetRenderer.getUrlTitle())}
		</#if>

		${printURL.setWindowState("pop_up")}

		<@liferay_ui["icon"]
			image="print"
			message="print"
			url="javascript:Liferay.Util.openWindow({id:'" + renderResponse.getNamespace() + "printAsset', title: '" + languageUtil.format(locale, "print-x-x", ["hide-accessible", entryTitle]) + "', uri: '" + htmlUtil.escapeURL(printURL.toString()) + "'});"
		/>
	</#if>
</#macro>

<#macro getRatings>
	<#if (enableRatings == "true")>
		<div class="asset-ratings">
			<@liferay_ui["ratings"]
				className=entry.getClassName()
				classPK=entry.getClassPK()
			/>
		</div>
	</#if>
</#macro>

<#macro getRelatedAssets>
	<#if enableRelatedAssets == "true">
		<@liferay_ui["asset-links"]
			assetEntryId=entry.getEntryId()
		/>
	</#if>
</#macro>

<#macro getSocialBookmarks>
	<#if enableSocialBookmarks == "true">
		<@liferay_ui["social-bookmarks"]
			displayStyle="${socialBookmarksDisplayStyle}"
			target="_blank"
			title=entry.getTitle(locale)
			url=viewURL
		/>
	</#if>
</#macro>