﻿<#if rsLink.getSiblings()?has_content>
    <h3 class="hide">${.vars['reserved-article-title'].data}</h3>
	<nav class="links-redes-sociais">
		<#list rsLink.getSiblings() as cur_rsLink>
			<#if cur_rsLink.rsNome.getData() == "icon-facebook-sign">
				<#assign rsTitle = "Facebook" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-youtube-sign">
				<#assign rsTitle = "Youtube" />
			<#elseif cur_rsLink.rsNome.getData() == "icone-soundcloud">
				<#assign rsTitle = "SoundCloud" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-twitter-sign">
				<#assign rsTitle = "Twitter" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-flickr">
				<#assign rsTitle = "Flickr" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-tumblr-sign">
				<#assign rsTitle = "Tumblr" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-google-plus-sign">
				<#assign rsTitle = "Google Plus" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-pinterest-sign">
				<#assign rsTitle = "Pinterest" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-linkedin-sign">
				<#assign rsTitle = "LinkedIn" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-rss-sign">
				<#assign rsTitle = "RSS" />
			<#elseif cur_rsLink.rsNome.getData() == "icon-instagram">
				<#assign rsTitle = "Instagram" />
			<#elseif cur_rsLink.rsNome.getData() == "icone-vimeo">
				<#assign rsTitle = "Vimeo" />
			<#elseif cur_rsLink.rsNome.getData() == "icone-picassa">
				<#assign rsTitle = "Picassa" />
			<#elseif cur_rsLink.rsNome.getData() == "icone-blogger">
				<#assign rsTitle = "Blogger" />
			<#else>
				<#assign rsTitle = "" />
			</#if>
			<a class="${cur_rsLink.rsNome.getData()}" target="_blanck" href="${cur_rsLink.getData()}" title="${rsTitle}">
				<span>${rsTitle}</span>
			</a>
		</#list>
	</nav>
</#if>